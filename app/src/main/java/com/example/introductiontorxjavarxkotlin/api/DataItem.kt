package com.example.introductiontorxjavarxkotlin.api

data class DataItem(
    val id: Int,
    val body: String,
    val title: String,
    val userId: Int
)