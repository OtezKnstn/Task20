package com.example.introductiontorxjavarxkotlin.api

import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface Interface {
    @GET("posts")
    fun getData(): Call<List<DataItem>>

    @FormUrlEncoded
    @POST("posts")
    suspend fun pushPost(
        @Field("id") id: Int,
        @Field("body") body: String,
        @Field("title") title: String,
        @Field("userId") userId: Int,
    ): Response<DataItem>
}