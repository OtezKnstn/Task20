package com.example.introductiontorxjavarxkotlin.rx

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.PublishSubject

open class RxBus {
    private lateinit var instance: RxBus
    private val publisher: PublishSubject<String> = PublishSubject.create()
    fun getInstance(): RxBus {
        if(!this::instance.isInitialized)
            instance = RxBus()

        return instance
    }

    fun publish(event: String) {
        Thread.sleep(500)
        publisher.onNext(event)
    }

    fun listen(): Observable<String> = publisher
}