package com.example.introductiontorxjavarxkotlin.ui.observer

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.introductiontorxjavarxkotlin.R
import com.example.introductiontorxjavarxkotlin.databinding.FragmentObserverBinding
import com.example.introductiontorxjavarxkotlin.rx.CurrentBus
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import kotlinx.coroutines.delay

class ObserverFragment : Fragment() {

    lateinit var binding: FragmentObserverBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /*
        -- setRetainInstance(true)
        ++ android:configChanges="screenSize|orientation"   ./AndroidManifest.xml
         */
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentObserverBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.button.setOnClickListener {
            CurrentBus.bus.listen().subscribe(createObserver());
        }
    }

    private fun createObserver(): Observer<String>{
        val observer: Observer<String> = object: Observer<String>{
            override fun onSubscribe(d: Disposable) {
                binding.textView.text = getString(R.string.subscribed)
            }

            override fun onNext(t: String) {
                binding.textView.text = t
            }

            override fun onError(e: Throwable) {
                binding.textView.text = getString(R.string.error)
            }

            override fun onComplete() {
                binding.textView.text = getString(R.string.complete)
            }
        }
        return observer
    }

    companion object {
        @JvmStatic
        fun newInstance() = ObserverFragment()
    }
}