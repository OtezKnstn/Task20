package com.example.introductiontorxjavarxkotlin.ui.observable

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.introductiontorxjavarxkotlin.R
import com.example.introductiontorxjavarxkotlin.api.DataItem
import com.example.introductiontorxjavarxkotlin.api.Interface
import com.example.introductiontorxjavarxkotlin.databinding.FragmentObservableBinding
import com.example.introductiontorxjavarxkotlin.rx.CurrentBus
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.IOException
import java.lang.reflect.Type
import kotlin.random.Random

class ObservableFragment : Fragment() {
    private val BASE_URL = "https://jsonplaceholder.typicode.com/"
    private lateinit var binding: FragmentObservableBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentObservableBinding.inflate(inflater)

        binding.fromAPI.setOnClickListener {
            try {
                getData()
                binding.ifSuccess.text = getString(R.string.from_api_success)
            } catch (e: IOException) {
                binding.ifSuccess.text = getString(R.string.parse_error)
            }

        }

        binding.fromFile.setOnClickListener {
            try {
                getDataFromJSONFile()
                binding.ifSuccess.text = getString(R.string.from_file_success)
            } catch (e: IOException) {
                binding.ifSuccess.text = getString(R.string.parse_error)
            }
        }

        return binding.root
    }


    private fun getData() {
        val retrofitBuilder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
            .create(Interface::class.java)

        val retrofitData = retrofitBuilder.getData()

        retrofitData.enqueue(object : Callback<List<DataItem>?> {
            override fun onResponse(
                call: Call<List<DataItem>?>,
                response: Response<List<DataItem>?>
            ) {
                val responseBody = response.body()!!
                CurrentBus.bus.publish(getRandomString(responseBody))
            }

            override fun onFailure(call: Call<List<DataItem>?>, t: Throwable) {

            }
        })
    }

    private fun getDataFromJSONFile() {
        try {
            val inputStream = requireContext().assets.open("data.json")
            val bufferedReader = inputStream.bufferedReader()
            val stringBuilder = StringBuilder()
            var line: String? = bufferedReader.readLine()
            while (line != null) {
                stringBuilder.append(line).append("\n")
                line = bufferedReader.readLine()
            }

            bufferedReader.close()
            val response = stringBuilder.toString()
            val listType: Type = object: TypeToken<List<DataItem>>(){}.type
            val listOfMessage: List<DataItem> = Gson().fromJson(response, listType)
            CurrentBus.bus.publish(getRandomString(listOfMessage))
        } catch (e:IOException) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show()
            throw e
        }
    }

    fun getRandomString(list: List<DataItem>): String{
        val i: Int = Random.nextInt(0, list.size - 1)
        return list[i].body
    }

    companion object {
        @JvmStatic
        fun newInstance() = ObservableFragment()
    }
}