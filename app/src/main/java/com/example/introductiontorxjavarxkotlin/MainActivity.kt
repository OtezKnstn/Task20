package com.example.introductiontorxjavarxkotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.introductiontorxjavarxkotlin.databinding.ActivityMainBinding
import com.example.introductiontorxjavarxkotlin.ui.observable.ObservableFragment
import com.example.introductiontorxjavarxkotlin.ui.observer.ObserverFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(this.layoutInflater)
        setContentView(binding.root)
        val navView: BottomNavigationView = binding.navView
        var firstInstance = true

        navView.setOnItemSelectedListener {
            when(it.itemId) {
                R.id.launch -> {
                    supportFragmentManager
                        .beginTransaction()
                        .let { ft ->
                            if (firstInstance) {
                                return@let ft.add(R.id.nav_host_fragment_activity_main, ObserverFragment.newInstance())
                            } else {
                                return@let ft
                                    .hide(ObservableFragment.newInstance())
                                    .show(ObserverFragment.newInstance())
                            }
                        }
                        .commit()
                }
                else -> {
                    supportFragmentManager
                        .beginTransaction()
                        .let { ft ->
                            if (firstInstance) {
                                firstInstance = false
                                return@let ft.add(R.id.nav_host_fragment_activity_main, ObservableFragment.newInstance())
                            } else {
                                return@let ft.show(ObservableFragment.newInstance())
                            }
                        }
                        .hide(ObserverFragment.newInstance())
                        .commit()
                }
            }

            return@setOnItemSelectedListener true
        }
    }
}